#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>
#include <qcustomplot.h>
#include <qaudioinput.h>
#include "MonoInput.h"

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:
		BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		void mp_InitPlot();

		void InitMicrophone();

		void stopMicrophone();

		~BaseScene();

  public slots:
	  void mp_StartButton();
      void mp_BackButton();
	  void mp_StopButton();
	  void mf_PlotRandom();
	
  private:
		
		QWidget* centralWidget;
		QCustomPlot* micPlot;
		QTimer* timer;
		QAudioInput* mAudioInput;
		MonoInput* mMonoInput;
		
		double mv_dfRefreshRate;
		double mv_dfDisplayTime;
		unsigned int mv_nSampleRate;

		QVector<double> xAxis, yAxis;

		

	};
}
