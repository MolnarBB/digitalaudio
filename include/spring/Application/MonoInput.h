#pragma once
#include <qiodevice.h>
#include <qaudioformat.h>
#include <qvector.h>

namespace Spring
{
	class MonoInput : public QIODevice
	{
	public:
		MonoInput(double sampleRate, int numberOfSamples);
		qint64 readData(char* data, qint64 maxlen) override;
		qint64 writeData(const char* data, qint64 len) override;
		
		QAudioFormat getAudioFormat();
		QVector<double> getVec() const;

	private:
		QAudioFormat mAudioFormat;
		QVector<double> mSamples;

		unsigned int mv_nSampleRate;
		int mv_nNumberOfSamples;
		int mv_nChannelBytes;
		int mv_nMaxAmplitude;
	};
}