#include <spring\Application\BaseScene.h>
#include <random>
#include <qaudiodeviceinfo.h>

#include "ui_base_scene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}
	void BaseScene::createScene()
	{
		//create the UI
		const auto ui = std::make_shared<Ui_baseScene>();
		ui->setupUi(m_uMainWindow.get());

    //connect btn's release signal to defined slot
    QObject::connect(ui->backButton, SIGNAL(released()), this, SLOT(mp_BackButton()));
	QObject::connect(ui->startButton, &QPushButton::released, this, &BaseScene::mp_StartButton);
	QObject::connect(ui->stopButton, &QPushButton::released, this, &BaseScene::mp_StopButton);

		//setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("new title goes here"));

		//setting centralWidget
		centralWidget = ui->centralwidget;

		//Get the title from transient data
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);
		mv_dfRefreshRate = boost::any_cast<double>(m_TransientDataCollection["RefreshRate"]);
		mv_dfDisplayTime = boost::any_cast<double>(m_TransientDataCollection["DisplayTime"]);
		mv_nSampleRate = boost::any_cast<unsigned int>(m_TransientDataCollection["SampleRate"]);

		int lv_nNumberOfSamples = mv_nSampleRate * mv_dfDisplayTime;
		mMonoInput = new MonoInput(mv_nSampleRate, lv_nNumberOfSamples);
		double lv_dfTimeIncrement = 1. / mv_nSampleRate;

		for (int i = 0; i < lv_nNumberOfSamples; i++)
		{
			xAxis.push_back(i * lv_dfTimeIncrement);
		}

		timer = new QTimer(this);
		QObject::connect(timer, &QTimer::timeout, this, &BaseScene::mf_PlotRandom);
		
		m_uMainWindow->setWindowTitle(QString(appName.c_str()));
		micPlot = ui->widget;

		mp_InitPlot();

	}

	void BaseScene::release()
	{
		delete centralWidget;
	}

	void BaseScene::InitMicrophone()
	{
		if (!mMonoInput->isOpen());
		{	
			mMonoInput->open(QIODevice::WriteOnly);
			
			QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());

			QAudioFormat format = mMonoInput->getAudioFormat();
			if (!info.isFormatSupported(format))
				format = info.nearestFormat(format);
			mAudioInput = new QAudioInput(mMonoInput->getAudioFormat(), this);
			mAudioInput->start(mMonoInput);
		}

	}

	void BaseScene::stopMicrophone()
	{
		if (mMonoInput->isOpen())
		{
			mAudioInput->stop();
			mMonoInput->close();
			delete mAudioInput;
		}
	}

	void BaseScene::mp_InitPlot()
	{
		micPlot->addGraph();
		micPlot->xAxis->setLabel("sec");
		micPlot->yAxis->setLabel("volts");
		micPlot->setInteraction(QCP::Interaction::iRangeZoom, true);

	}

	void BaseScene::mf_PlotRandom()
	{

	yAxis = mMonoInput->getVec();
	xAxis.resize(yAxis.size());


	micPlot->graph(0)->setData(xAxis, yAxis);
	micPlot->graph(0)->rescaleAxes();
	micPlot->replot();

	}

	BaseScene::~BaseScene()
	{

	}

	void BaseScene::mp_StartButton()
	{
		auto mv_dInterval = (1 / mv_dfRefreshRate) * 1000;
		if (!timer->isActive()) 
		{
			timer->start(mv_dInterval);
			InitMicrophone();
		}
	}

  void BaseScene::mp_BackButton()
  {
    const std::string c_szNextSceneName = "Initial scene";
    emit SceneChange(c_szNextSceneName);
  }

  void BaseScene::mp_StopButton()
  {
	  if (timer->isActive())
	  {
		  timer->stop();
		  micPlot->graph(0)->data()->clear();
		  micPlot->replot();
		  stopMicrophone();
		  //micPlot->clearGraphs();
	  }
  }

}
