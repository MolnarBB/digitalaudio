#include <spring/Application/MonoInput.h>
#include <iostream>
#include <qendian.h>


namespace Spring
{
	MonoInput::MonoInput(double sampleRate, int numberOfSamples):
		mv_nSampleRate(sampleRate)
	{
		mAudioFormat = QAudioFormat();
		mAudioFormat.setSampleRate(sampleRate);
		mAudioFormat.setChannelCount(1);
		mAudioFormat.setSampleSize(16);
		mAudioFormat.setSampleType(QAudioFormat::SignedInt);
		mAudioFormat.setByteOrder(QAudioFormat::LittleEndian);
		mAudioFormat.setCodec("audio/pcm");
		mv_nChannelBytes = 2;
		mv_nMaxAmplitude = 32767;
		mv_nNumberOfSamples = numberOfSamples;
	}

	qint64 MonoInput::readData(char*data, qint64 maxlen)
	{
		Q_UNUSED(data);
		Q_UNUSED(maxlen);
		return -1;
	}
	
	qint64 MonoInput::writeData(const char* data, qint64 len)
	{
		const auto *ptr = reinterpret_cast<const unsigned char*>(data);
		
		for (int i = 0; i < len/mv_nChannelBytes; i++)
		{
			qint32 value;
			value = qFromLittleEndian<qint16>(ptr);
			double lv_dfSample;
			lv_dfSample = value * (1.736 / mv_nMaxAmplitude);
			mSamples.push_back(lv_dfSample);
			ptr += mv_nChannelBytes;
		}
		
		if (mSamples.size() > mv_nNumberOfSamples)
		{
			mSamples.remove(0, mSamples.size() - mv_nNumberOfSamples);
		}
		return len;
	}

	QAudioFormat MonoInput::getAudioFormat()
	{
		return mAudioFormat;
	}

	QVector<double> MonoInput::getVec() const
	{
		return mSamples;
	}
	

}
